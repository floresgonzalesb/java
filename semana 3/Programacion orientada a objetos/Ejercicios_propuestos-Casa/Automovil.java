import java.util.ArrayList;

public class Automovil {
    String marca;

    int modelo;

    boolean Automatico;

    int motor;

    int Multa;
    
    enum tipoCom {GASOLINA, BIOETANOL, DIESEL, BIODISESEL, GAS_NATURAL}
    
    tipoCom tipoCombustible;
    
    enum tipoA {CIUDAD, SUBCOMPACTO, COMPACTO, FAMILIAR, EJECUTIVO, SUV}
    
    tipoA tipoAutomovil;
    
    int numeroPuertas;
    
    int cantidadAsientos;
    
    int velocidadMaxima;
    
    enum tipoColor {BLANCO, NEGRO, ROJO, NARANJA, AMARILLO, VERDE, AZUL, VIOLETA}
    
    tipoColor color;
    
    int velocidadActual = 0;


    Automovil(String marca, int modelo, String string, int motor, tipoCom tipoCombustible, tipoA tipoAutomovil, int numeroPuertas, int cantidadAsientos, int velocidadMaxima, tipoColor color, int Multa) {
        this.marca = marca;
        this.modelo = modelo;
        this.motor = motor;
        this.tipoCombustible = tipoCombustible;
        this.tipoAutomovil = tipoAutomovil;
        this.numeroPuertas = numeroPuertas;
        this.cantidadAsientos = cantidadAsientos;
        this.velocidadMaxima = velocidadMaxima;
        this.color = color;
        this.Automatico = Automatico;
        this.Multa = 0; 
        
    }

    String getMarca() {
        return marca;
    }

    int getMulta(){
        return Multa;
    }

    int getModelo() {
        return modelo;
    }

    int getMotor() {
        return motor;
    }

    tipoCom getTipoCombustible() {
        return tipoCombustible;
    }

    tipoA getTipoAutomovil() {
        return tipoAutomovil;
    }

    int getNumeroPuertas() {
        return numeroPuertas;
    }

    int getCantidadAsientos() {
        return cantidadAsientos;
    }

    int getVelocidadMaxima() {
        return velocidadMaxima;
    }

    tipoColor getColor() {
        return color;
    }

    int getVelocidadActual() {
        return velocidadActual;
    }
    
    boolean getAutomatico(){
        return Automatico;
    }

    int tieneMultas() {
        return Multa;
    }

    void setMarca(String marca) {
        this.marca = marca;
    }
    
    void setModelo(int modelo) {
        this.modelo = modelo;
    }
    
    void setMotor(int motor) {
        this.motor = motor;
    }

    void setTipoCombustible(tipoCom tipoCombustible) {
        this.tipoCombustible = tipoCombustible;
    }
    
    void setTipoAutomovil(tipoA tipoAutomovil) {
        this.tipoAutomovil = tipoAutomovil;
    }
    
    void setNumeroPuertas(int numeroPuertas) {
        this.numeroPuertas = numeroPuertas;
    }

    void setCantidadAsientos(int cantidadAsientos) {
        this.cantidadAsientos = cantidadAsientos;
    }

    void setVelocidadMaxima(int velocidadMaxima) {
        this.velocidadMaxima = velocidadMaxima;
    }

    void setColor(tipoColor color) {
        this.color = color;
    }


    void setVelocidadActual(int velocidadActual) {
        this.velocidadActual = velocidadActual;
    }

    void setAutomatico(boolean Automatico){
        this.Automatico = Automatico;
    }

    void setMulta(int Multa){
        this.Multa = Multa;
    }


    public void acelerar(int incremento) {
        if (velocidadActual + incremento > velocidadMaxima) {
            add(incremento);
            System.out.println("Multa generada por sobrepasar la velocidad máxima permitida.");
        }
        velocidadActual += incremento;
    }

    private void add(int incremento) {
    }

    void desacelerar(int decrementoVelocidad) {

        if ((velocidadActual - decrementoVelocidad) > 0) {
            velocidadActual = velocidadActual - decrementoVelocidad;
        } else { 
            System.out.println("No se puede decrementar a una velocidad negativa.");
        }
    }

    public void frenar(int decremento) {
        velocidadActual -= decremento;
        if (velocidadActual < 0) {
            velocidadActual = 0;
        }
    }

    double calcularTiempoLlegada(int distancia) {
        return distancia/velocidadActual;
    }

    void imprimir() {
        System.out.println("Marca = "  + marca);
        System.out.println("Modelo = "  + modelo);
        System.out.println("Motor = "  + motor);
        System.out.println("Tipo de combustible = "  + tipoCombustible);
        System.out.println("Tipo de automóvil = "  + tipoAutomovil);
        System.out.println("Número de puertas = "  + numeroPuertas);
        System.out.println("Cantidad de asientos = " + cantidadAsientos);
        System.out.println("Velocida máxima = "  + velocidadMaxima);
        System.out.println("Color = "  + color);
        System.out.println("Cantidad de  multas = "+Multa);
        System.out.println("Es vehiculo es Automatico? "+Automatico);
    }

    public static void main(String args[]) {
        Automovil auto1 = new Automovil("ford",2018,"NO",3,tipoCom.DIESEL,tipoA.EJECUTIVO,5,6,250,tipoColor.AZUL,2);
        auto1.imprimir();
        auto1.setVelocidadActual(100);
        System.out.println("Velocidad actual = " + auto1.velocidadActual);
        auto1.acelerar(20);
        System.out.println("Velocidad actual = " + auto1.velocidadActual);
        auto1.desacelerar(50);
        System.out.println("Velocidad actual = " + auto1.velocidadActual);
        auto1.frenar(0);
        System.out.println("Velocidad actual = " + auto1.velocidadActual);
        auto1.desacelerar(20);
    }
}
