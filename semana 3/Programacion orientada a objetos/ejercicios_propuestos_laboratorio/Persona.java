public class Persona {
    String nombre; 
    String apellidos;
    String numeroDocumentoIdentidad;
    String PaisNacimiento;
    int añoNacimiento;
    char genero;
    
    
    Persona(String nombre, String apellidos, String numeroDocumentoIdentidad, String PaisNacimiento, int añoNacimiento, char genero) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.numeroDocumentoIdentidad = numeroDocumentoIdentidad;
        this.añoNacimiento = añoNacimiento;
        this.PaisNacimiento = PaisNacimiento;
        this.genero = genero;
    }
    void Imprimir(){
      System.out.println("Nombre: "+nombre);
      System.out.println("Apellido: "+apellidos);
      System.out.println("Numero de documento: "+ numeroDocumentoIdentidad);
      System.out.println("Año de nacimiento: "+añoNacimiento);
      System.out.println("Genero: "+genero);
      System.out.println("Pais de nacimiento: "+PaisNacimiento);
    }
    public static void main(String[] args) {
      Persona persona1 = new Persona("Ronald", "Oxenforge", "7489516", "Perú", 1999, 'H');     
      Persona persona2 = new Persona("Maria", "Chiline", "87971879214", "Argentina", 1999, 'M');
      persona1.Imprimir();
      persona2.Imprimir();

  }
}