import java.util.Scanner;

public class ejercicio3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese el número de valores: ");
        int n = sc.nextInt();
        int[] num = new int[n];
        for (int i = 0; i < n; i++) {
            System.out.print("Ingrese el valor " + (i+1) + ": ");
            num[i] = sc.nextInt();
        }

        int resultado = MCM(num, n);

        System.out.println("El MCM de los " + n + " valores es: " + resultado);
    }

    public static int MCM(int[] num, int n) {
        if (n == 1) {
            return num[0];
        } else {
            int[] subArray = new int[n-1];
            System.arraycopy(num, 0, subArray, 0, n-1);
            return mcm(num[n-1], MCM(subArray, n-1));
        }
    }

    public static int mcm(int num1, int num2) {
        return (num1 * num2) / mcd(num1, num2);
    }

    public static int mcd(int num1, int num2) {
        if (num2 == 0) {
            return num1;
        } else {
            return mcd(num2, num1 % num2);
        }
    }
}
