import java.util.Scanner;

public class ejercicio2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese el primer número: ");
        int num1 = sc.nextInt();
        System.out.print("Ingrese el segundo número: ");
        int num2 = sc.nextInt();

        int resultado = MCD(num1, num2);

        System.out.println("El MCD de " + num1 + " y " + num2 + " es: " + resultado);
    }

    public static int MCD(int num1, int num2) {
        if (num2 == 0) {
            return num1;
        } else {
            return MCD(num2, num1 % num2);
        }
    }
}
