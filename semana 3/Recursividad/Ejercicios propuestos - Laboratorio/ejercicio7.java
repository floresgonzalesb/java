public class ejercicio7 {

    public void ordenar(int[] arreglo, int inicio, int fin) {
        if (inicio < fin) {
            int indiceParticion = particionar(arreglo, inicio, fin);
            ordenar(arreglo, inicio, indiceParticion - 1);
            ordenar(arreglo, indiceParticion + 1, fin);
        }
    }

    private int particionar(int[] arreglo, int inicio, int fin) {
        int pivote = arreglo[fin];
        int indiceMenor = inicio - 1;
        for (int i = inicio; i < fin; i++) {
            if (arreglo[i] <= pivote) {
                indiceMenor++;
                intercambiar(arreglo, indiceMenor, i);
            }
        }
        intercambiar(arreglo, indiceMenor + 1, fin);
        return indiceMenor + 1;
    }

    private void intercambiar(int[] arreglo, int i, int j) {
        int temp = arreglo[i];
        arreglo[i] = arreglo[j];
        arreglo[j] = temp;
    }

    public static void main(String[] args) {
        int[] arreglo = {5, 2, 7, 1, 8, 4};
        ejercicio7 qs = new ejercicio7();
        qs.ordenar(arreglo, 0, arreglo.length - 1);
        for (int i = 0; i < arreglo.length; i++) {
            System.out.print(arreglo[i] + " ");
        }
    }
}

