public class ejercicio4 {

    public static int calculoP(int n) {
        if (n == 1) {
            return 1;
        } else {
            return n * calculoP(n - 1);
        }
    }

    public static void main(String[] args) {
        int n = 5;
        int numPermutaciones = calculoP(n);
        System.out.println("El número de permutaciones de una lista de " + n + " elementos es: " + numPermutaciones);
    }
}
