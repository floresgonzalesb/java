public class ejercicio_3 {
    
    public static int ackermann(int m, int n) {
        if (m == 0) {
            return n + 1;
        } else if (n == 0) {
            return ackermann(m-1, 1);
        } else {
            return ackermann(m-1, ackermann(m, n-1));
        }
    }
    
    public static void main(String[] args) {
        int m = 3;
        int n = 4;
        int resultado = ackermann(m, n);
        System.out.println("El resultado de la secuencia de Ackermann para m = " + m + ", n = " + n + " es: " + resultado);
    }
}
