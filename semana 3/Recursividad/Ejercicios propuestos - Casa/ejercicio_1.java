import java.util.Scanner;

public class ejercicio_1 {
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in); 
        
        System.out.print("Ingrese la base: ");
        int b = sc.nextInt(); 
        
        System.out.print("Ingrese el exponente: ");
        int e = sc.nextInt(); 
        
        int resultado = potencia(b, e);
        System.out.println(b + " elevado a la " + e + " es igual a " + resultado);
        
    }
    
    public static int potencia(int b, int e) {
        if (e == 0) {
            return 1;
        } else {
            return b * potencia(b, e-1);
        }
    }
}
