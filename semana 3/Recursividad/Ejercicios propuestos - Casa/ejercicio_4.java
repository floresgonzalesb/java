public class ejercicio_4 {
    
    public static void imprimirInverso(int n) {
        if (n == 1) {
            System.out.print(n + " ");
        } else {
            System.out.print(n + " ");
            imprimirInverso(n-1);
        }
    }
    
    public static void main(String[] args) {
        int n = 10;
        System.out.println("Los números del 1 al " + n + " en orden inverso son: ");
        imprimirInverso(n);
    }
}
