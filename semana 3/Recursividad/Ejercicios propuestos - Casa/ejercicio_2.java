public class ejercicio_2 {

    public static int MValor(int[] array, int n) {
        if (n == 1) {
            return array[0];
        } else {
            int max = MValor(array, n-1);
            return Math.max(max, array[n-1]);
        }
    }

    public static void main(String[] args) {
        int[] array = {7, 32, 10, 5, 22, 17};
        int max = MValor(array, array.length);
        System.out.println("El valor máximo es: " + max);
    }
}
