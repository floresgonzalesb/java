public class ejercicio_5 {

    public static void dibujo(int n, int longitud) {
        if (n > 0) {
            dibujo(n - 1, longitud);
            for (int i = 0; i < longitud - n; i++) {
                System.out.print(" ");
            }
            for (int i = 0; i < n; i++) {
                System.out.print("*");
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        int n = 7;
        dibujo(n, n);
    }
}
