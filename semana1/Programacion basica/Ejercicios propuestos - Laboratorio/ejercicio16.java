import java.util.Scanner;

public class ejercicio16 {
    public static void main(String[] args) {
        Scanner corporal = new Scanner(System.in);
        System.out.print("Ingrese su peso en kilogramos: ");
        double peso = corporal.nextDouble();
        System.out.print("Ingrese su altura en metros: ");
        double altura = corporal.nextDouble();
        double imc = peso / (altura * altura);
        System.out.printf("Su índice de masa corporal es %.2f", imc);
        corporal.close();
    }
}
