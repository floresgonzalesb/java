import java.util.Scanner;

public class ejercicio13 {
    public static void main(String[] args) {
        Scanner cadena = new Scanner(System.in);
        
        System.out.print("Ingrese una cadena de texto: ");
        String texto = cadena.nextLine();
        
        System.out.println("La cadena de texto ingresada es: " + texto);
        
        cadena.close();
    }
}
