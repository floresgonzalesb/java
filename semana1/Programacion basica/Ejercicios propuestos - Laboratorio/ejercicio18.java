import java.util.Scanner;

public class ejercicio18 {
    public static void main(String[] args) {
        Scanner numero = new Scanner(System.in);
        System.out.print("Ingrese un número entero positivo: ");
        int num = numero.nextInt();
        int factorial = 1;
        if (num < 0) {
            System.out.println("Error: el número debe ser positivo.");
        } else {
            for (int i = 1; i <= num; i++) {
                factorial *= i;
            }
            System.out.println(num + "! = " + factorial);
        }
        numero.close();
    }
}
