import java.util.Scanner;

public class ejercicio08 {
    public static void main(String[] args) {
        Scanner triangulo = new Scanner(System.in);

        System.out.println("Ingrese la longitud de un lado");
        float lado1 = triangulo.nextInt();

        System.out.println("Ingrese la longitud de un lado");
        float lado2 = triangulo.nextInt();

        System.out.println("Ingrese la longitud de un lado");
        float lado3 = triangulo.nextInt();

        if (lado1 == lado2 && lado2 == lado3) {
            System.out.println("El triángulo es equilátero.");
        }
        else if (lado1 == lado2 || lado1 == lado3 || lado2 == lado3) {
            System.out.println("El triángulo es isósceles.");
        }
        else {
            System.out.println("El triángulo es escaleno.");
        }
    }
}
