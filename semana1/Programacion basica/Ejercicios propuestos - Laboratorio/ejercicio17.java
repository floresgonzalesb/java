import java.util.Scanner;

public class ejercicio17 {
    public static void main(String[] args) {
        Scanner numero = new Scanner(System.in);
        System.out.print("Ingrese un número entero: ");
        int num = numero.nextInt();
        if (num > 0) {
            System.out.println("El número es positivo.");
        } else if (num < 0) {
            System.out.println("El número es negativo.");
        } else {
            System.out.println("El número es cero.");
        }
        numero.close();
    }
}
