import java.util.Scanner;

public class ejercicio12 {
    public static void main(String[] args) {
        Scanner numero = new Scanner(System.in);
        
        System.out.print("Ingrese el primer número: ");
        int num1 = numero.nextInt();
        
        System.out.print("Ingrese el segundo número: ");
        int num2 = numero.nextInt();
        
        if (num1 == num2) {
            System.out.println("Los números son iguales");
        } else {
            System.out.println("Los números son diferentes");
        }
        
        numero.close();
    }
}
