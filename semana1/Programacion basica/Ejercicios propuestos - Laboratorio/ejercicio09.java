import java.util.Scanner;

public class ejercicio09 {
    public static void main(String[] args) {
        double radio, area, circunferencia;
        final double PI = 3.14159; // Valor constante de PI

        Scanner circulo = new Scanner(System.in);

        System.out.print("Ingresa el radio del círculo: ");
        radio = circulo.nextDouble();

        area = PI * Math.pow(radio, 2);
        circunferencia = 2 * PI * radio;

        System.out.println("El área del círculo es: " + area);
        System.out.println("La circunferencia del círculo es: " + circunferencia);
    }
}
