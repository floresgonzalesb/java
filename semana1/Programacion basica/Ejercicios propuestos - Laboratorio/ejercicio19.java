import java.util.Scanner;

public class ejercicio19 {
    public static void main(String[] args) {
        Scanner numero = new Scanner(System.in);
        System.out.print("Ingrese un número entero: ");
        int n = numero.nextInt();
        int suma = 0;
        for (int i = 1; i <= n; i++) {
            suma += i;
        }
        System.out.println("La suma de todos los números enteros desde 1 hasta " + n + " es " + suma);

        numero.close();
    }
}
