import java.util.Scanner;
public class ejercicio05 {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        System.out.println("Ingrese el primer numero entero");
        int num1 = entrada.nextInt();

        System.out.println("Ingrese el segundo numero entero");
        int num2 = entrada.nextInt();

        int diferencia  = num1 - num2 ;

        System.out.println("La diferencia de "+ num1 + " y " + num2 + " es " + diferencia);
        
        
    }
}
