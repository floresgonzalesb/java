import java.util.Scanner;

public class ejercicio15 {
    public static void main(String[] args) {
        Scanner numero = new Scanner(System.in);
        System.out.print("Ingrese un número entero: ");
        int num = numero.nextInt();
        boolean esPrimo = true;
        
        if (num <= 1) {
            esPrimo = false;
        } else {
            for (int i = 2; i <= num/2; i++) {
                if (num % i == 0) {
                    esPrimo = false;
                    break;
                }
            }
        }
        
        if (esPrimo) {
            System.out.println(num + " es un número primo.");
        } else {
            System.out.println(num + " no es un número primo.");
        }
        
        numero.close();
    }
}
