import java.util.Scanner;

public class ejercicio20 {
    public static void main(String[] args) {
        Scanner numero = new Scanner(System.in);
        System.out.print("Ingrese el primer número: ");
        int a = numero.nextInt();
        System.out.print("Ingrese el segundo número: ");
        int b = numero.nextInt();
        
        int mcd = calcularMCD(a, b);
        int mcm = a * b / mcd;
        
        System.out.println("El Máximo Común Divisor (MCD) de " + a + " y " + b + " es " + mcd);
        System.out.println("El Mínimo Común Múltiplo (MCM) de " + a + " y " + b + " es " + mcm);
    }
    
    public static int calcularMCD(int a, int b) {
        if (b == 0) {
            return a;
        } else {
            return calcularMCD(b, a % b);

        }
    }
}