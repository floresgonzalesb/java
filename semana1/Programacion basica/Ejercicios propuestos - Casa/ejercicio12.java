import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ejercicio12 {
    public static void main(String[] args) {
        Date fechaActual = new Date();
        DateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
        DateFormat formatoHora = new SimpleDateFormat("HH:mm:ss");

        String fechaFormateada = formatoFecha.format(fechaActual);
        String horaFormateada = formatoHora.format(fechaActual);

        System.out.println("Fecha actual: " + fechaFormateada);
        System.out.println("Hora actual: " + horaFormateada);
    }
}
