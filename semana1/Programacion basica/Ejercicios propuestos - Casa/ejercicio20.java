import java.io.IOException;
import java.net.*;

public class ejercicio20 {
    public static void main(String[] args) {
        try {
            Socket socket = new Socket("www.example.com", 80);
            System.out.println("Conexión establecida con éxito");
            socket.close();
        } catch (UnknownHostException e) {
            System.out.println("Host desconocido: " + e.getMessage());
        } catch (IOException e) {
            System.out.println("Error al establecer la conexión: " + e.getMessage());
        }
    }
}
