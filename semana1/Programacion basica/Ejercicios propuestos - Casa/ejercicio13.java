import java.text.SimpleDateFormat;
import java.util.Date;

public class ejercicio13 {
    public static void main(String[] args) {
        Date fechaActual = new Date();
        SimpleDateFormat formatoPersonalizado = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS");

        String fechaFormateada = formatoPersonalizado.format(fechaActual);

        System.out.println("Fecha actual formateada: " + fechaFormateada);
    }
}
