import java.util.Locale;

public class ejercicio17 {
    public static void main(String[] args) {
        Locale configuracionRegional = new Locale("es", "MX");

        System.out.println("Idioma: " + configuracionRegional.getLanguage());
        System.out.println("País: " + configuracionRegional.getCountry());
        System.out.println("Nombre descriptivo: " + configuracionRegional.getDisplayName());
    }
}
