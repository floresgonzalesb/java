import java.util.Scanner;

public class ejercicio06 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Ingresa un nombre: ");
        String nombre = scanner.nextLine();
        System.out.println("Hola " + nombre);
        scanner.close();
    }
}
