import java.io.*;

public class ejercicio18 {
    public static void main(String[] args) {
        try {
            InputStream entrada = new FileInputStream("archivo.txt");
            int dato;

            while ((dato = entrada.read()) != -1) {
                System.out.print((char) dato);
            }

            entrada.close();
        } catch (IOException e) {
            System.out.println("Error al leer el archivo: " + e.getMessage());
        }
    }
}
