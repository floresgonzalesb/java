import java.math.BigDecimal;

public class ejercicio14 {
    public static void main(String[] args) {
        BigDecimal num1 = new BigDecimal("12345678901234567890.12345678901234567890");
        BigDecimal num2 = new BigDecimal("98765432109876543210.98765432109876543210");

        BigDecimal suma = num1.add(num2);
        BigDecimal resta = num2.subtract(num1);
        BigDecimal multiplicacion = num1.multiply(num2);
        BigDecimal division = num2.divide(num1, 25, BigDecimal.ROUND_HALF_UP);

        System.out.println("Suma: " + suma);
        System.out.println("Resta: " + resta);
        System.out.println("Multiplicación: " + multiplicacion);
        System.out.println("División: " + division);
    }
}
