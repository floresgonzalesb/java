import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ejercicio11 {
    public static void main(String[] args) {
        String cadena = "Hola, mi nombre es Juan y tengo 30 años.";
        Pattern patron = Pattern.compile("\\d+");
        Matcher matcher = patron.matcher(cadena);
        if (matcher.find()) {
            System.out.println("El patrón fue encontrado en la posición " + matcher.start() + " - " + matcher.end());
        } else {
            System.out.println("El patrón no fue encontrado.");
        }
    }
}
