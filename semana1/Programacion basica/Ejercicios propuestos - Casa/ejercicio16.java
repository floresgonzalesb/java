import java.util.TimeZone;

public class ejercicio16 {
    public static void main(String[] args) {
        TimeZone zonaHoraria = TimeZone.getTimeZone("America/Mexico_City");

        System.out.println("Zona horaria: " + zonaHoraria.getDisplayName());
        System.out.println("ID de zona horaria: " + zonaHoraria.getID());
        System.out.println("Horas de desplazamiento con respecto a GMT: " + zonaHoraria.getRawOffset() / (60 * 60 * 1000));
    }
}
