import java.util.regex.*;

public class ejercicio10 {
    public static void main(String[] args) {
        String regex = "\\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Z]{2,}\\b";
        Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE); 
        
        String texto = "Mi correo es example@domain.com y mi otro correo es correo@otrodominio.com"; 

        Matcher matcher = pattern.matcher(texto); 
        
        while (matcher.find()) {
            System.out.println("Correo encontrado: " + matcher.group()); 
        }
    }
}
