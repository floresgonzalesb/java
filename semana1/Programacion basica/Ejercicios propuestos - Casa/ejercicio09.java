public class ejercicio09 {
    public static void main(String[] args) {
        double num1 = 4.5;
        double num2 = 2.3;
        
        double suma = Math.addExact(3, 4); 
        double resta = num1 - num2; 
        double multiplicacion = Math.multiplyExact(5, 6);
        double division = num1 / num2;
        
        System.out.println("La suma de 3 y 4 es: " + suma);
        System.out.println("La resta de " + num1 + " y " + num2 + " es: " + resta);
        System.out.println("La multiplicación de 5 y 6 es: " + multiplicacion);
        System.out.println("La división de " + num1 + " y " + num2 + " es: " + division);
    }
}
