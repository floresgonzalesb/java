import java.io.*;

public class ejercicio19 {
    public static void main(String[] args) {
        try {
            OutputStream salida = new FileOutputStream("archivo.txt");
            String mensaje = "Hola, mundo!";
            byte[] buffer = mensaje.getBytes();

            salida.write(buffer);
            salida.close();
        } catch (IOException e) {
            System.out.println("Error al escribir en el archivo: " + e.getMessage());
        }
    }
}
