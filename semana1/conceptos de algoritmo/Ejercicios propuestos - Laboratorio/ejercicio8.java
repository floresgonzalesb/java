import java.util.Scanner;

public class ejercicio8 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Ingrese una fecha en formato dd/mm/yyyy: ");
        String fecha = input.nextLine();
        String[] partesFecha = fecha.split("/");
        int dia = Integer.parseInt(partesFecha[0]);
        int mes = Integer.parseInt(partesFecha[1]);
        int anio = Integer.parseInt(partesFecha[2]);
        boolean esFechaValida = false;
        if (anio >= 1900 && anio <= 9999) {
            if (mes >= 1 && mes <= 12) {
                int[] diasPorMes = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
                if (esAnioBisiesto(anio)) {
                    diasPorMes[1] = 29;
                }
                if (dia >= 1 && dia <= diasPorMes[mes - 1]) {
                    esFechaValida = true;
                }
            }
        }
        if (esFechaValida) {
            System.out.println("La fecha es válida.");
        } else {
            System.out.println("La fecha no es válida.");
        }
    }
    
    public static boolean esAnioBisiesto(int anio) {
        if (anio % 4 == 0) {
            if (anio % 100 == 0) {
                if (anio % 400 == 0) {
                    return true;
                }
                return false;
            }
            return true;
        }
        return false;
    }
}
