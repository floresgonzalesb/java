import java.util.Scanner;

public class ejercicio17 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Introduce una longitud: ");
        int longitud = input.nextInt();
        System.out.print("Introduce una lista de palabras separadas por comas: ");
        String palabras = input.next();

        int contador = 0;
        for (String palabra : palabras.split(",")) {
            if (palabra.length() > longitud) {
                contador++;
            }
        }

        System.out.println("Hay " + contador + " palabras con longitud mayor que " + longitud);
    }
}

