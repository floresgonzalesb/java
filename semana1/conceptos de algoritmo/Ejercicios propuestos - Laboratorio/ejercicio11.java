import java.util.Scanner;

public class ejercicio11 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Introduce la cantidad de números a ingresar: ");
        int cantidad = sc.nextInt();
        
        int contadorMultiplos = 0;
        for(int i = 0; i < cantidad; i++){
            System.out.print("Introduce un número entero: ");
            int numero = sc.nextInt();
            if(numero % 3 == 0){
                contadorMultiplos++;
            }
        }
        System.out.println("La cantidad de números múltiplos de 3 es: " + contadorMultiplos);
    }
}
