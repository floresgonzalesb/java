import java.util.Scanner;

public class ejercicio13{
    public static int fibonacci(int n) {
        if (n <= 1) {
            return n;
        }
        return fibonacci(n-1) + fibonacci(n-2);
    }
public static void main(String[] args) {
    Scanner fibo = new Scanner(System.in);
    System.out.println("ingrese numeros enteros mediante una coma: ");
    String[] numerosString = fibo.nextLine().split(",");
        int[] numeros = new int[numerosString.length];
        for (int i = 0; i < numerosString.length; i++) {
            numeros[i] = Integer.parseInt(numerosString[i]);
        }
    int numerofibonacci = 0;
    for (int i = 0; i < 10; i++) {
        System.out.print(fibonacci(i) + " ");
        }
        System.out.println("Hay números fibonacci en la lista.");
        fibo.close();
    }
}   