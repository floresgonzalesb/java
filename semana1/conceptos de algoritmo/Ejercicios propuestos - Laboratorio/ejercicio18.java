import java.util.Scanner;

public class ejercicio18 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Introduce una longitud: ");
        int longitud = sc.nextInt();
        sc.nextLine(); 
        System.out.print("Introduce una lista de palabras separadas por comas: ");
        String[] palabras = sc.nextLine().split(",");

        int contador = 0;
        for (String palabra : palabras) {
            if (palabra.trim().length() < longitud) {
                contador++;
            }
        }

        System.out.println("Hay " + contador + " palabras con longitud menor que " + longitud);
    }
}
