import java.util.Scanner;

public class ejercicio16 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Introduce una letra: ");
        String letra = sc.nextLine();
        System.out.print("Introduce una lista de palabras separadas por comas: ");
        String[] palabras = sc.nextLine().split(",");

        int contador = 0;
        for (String palabra : palabras) {
            if (palabra.trim().contains(letra)) {
                contador++;
            }
        }

        System.out.println("Hay " + contador + " palabras que contienen la letra " + letra);
    }
}
