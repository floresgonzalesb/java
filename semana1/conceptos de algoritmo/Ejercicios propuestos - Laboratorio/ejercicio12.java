import java.util.Scanner;

public class ejercicio12 {

    public static boolean esPerfecto(int n) {
        int sumaDivisores = 0;
        for (int i = 1; i < n; i++) {
            if (n % i == 0) {
                sumaDivisores += i;
            }
        }
        return sumaDivisores == n;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese una lista de números enteros separados por comas: ");
        String[] numerosString = sc.nextLine().split(",");
        int[] numeros = new int[numerosString.length];
        for (int i = 0; i < numerosString.length; i++) {
            numeros[i] = Integer.parseInt(numerosString[i]);
        }
        int conteoPerfectos = 0;
        for (int i = 0; i < numeros.length; i++) {
            if (esPerfecto(numeros[i])) {
                conteoPerfectos++;
            }
        }
        System.out.println("Hay " + conteoPerfectos + " números perfectos en la lista.");

        sc.close();
    }
}


