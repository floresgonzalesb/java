import java.util.ArrayList;

public class ejercicio14 {
    public static boolean isPalindrome(String palabra) {
        String palabraInvertida = new StringBuilder(palabra).reverse().toString();
        return palabra.equals(palabraInvertida);
    }

    public static void main(String[] args) {
        ArrayList<String> palabras = new ArrayList<String>();
        palabras.add("anana");
        palabras.add("oso");
        palabras.add("casa");
        palabras.add("radar");
        palabras.add("hola");

        for (String palabra : palabras) {
            if (isPalindrome(palabra)) {
                System.out.println(palabra + " es un palíndromo");
            } else {
                System.out.println(palabra + " no es un palíndromo");
            }
        }
    }
}
