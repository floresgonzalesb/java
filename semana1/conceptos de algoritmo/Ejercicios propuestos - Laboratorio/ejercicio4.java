import java.util.Scanner;

public class ejercicio4 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Ingrese el primer número entero: ");
        int lado1 = input.nextInt();
        System.out.print("Ingrese el segundo número entero: ");
        int lado2 = input.nextInt();
        System.out.print("Ingrese el tercer número entero: ");
        int lado3 = input.nextInt();
        if (lado1 + lado2 > lado3 && lado2 + lado3 > lado1 && lado3 + lado1 > lado2) {
            System.out.println("Los tres números pueden formar un triángulo válido.");
        } else {
            System.out.println("Los tres números no pueden formar un triángulo válido.");
        }
    }
}
