import java.util.Scanner;

public class ejercicio10 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Ingrese los números separados por comas: ");
        String entrada = input.nextLine();
        String[] numerosString = entrada.split(",");
        int cantidadPares = 0;
        for (int i = 0; i < numerosString.length; i++) {
            int numero = Integer.parseInt(numerosString[i].trim());
            if (numero % 2 == 0) {
                cantidadPares++;
            }
        }
        System.out.println("La cantidad de números pares es: " + cantidadPares);
    }
}
