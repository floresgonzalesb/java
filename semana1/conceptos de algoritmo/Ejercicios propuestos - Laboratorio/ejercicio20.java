import java.util.Scanner;

public class ejercicio20 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Introduce una cantidad de divisores: ");
        int cantidadDivisores = sc.nextInt();
        sc.nextLine();
        System.out.print("Introduce una lista de números enteros separados por comas: ");
        String[] numeros = sc.nextLine().split(",");

        int contador = 0;
        for (String numeroStr : numeros) {
            int numero = Integer.parseInt(numeroStr.trim());
            int contadorDivisores = 0;
            for (int i = 1; i <= numero; i++) {
                if (numero % i == 0) {
                    contadorDivisores++;
                }
            }
            if (contadorDivisores > cantidadDivisores) {
                contador++;
            }
        }

        System.out.println("Hay " + contador + " números con más de " + cantidadDivisores + " divisores.");
    }
}
