public class ejer9 {
    public static void main(String[] args) {
        String[] palabras = {"hola", "mundo", "programacion", "algoritmo", "computadora"};
        int contador = 0;
        for (String palabra : palabras) {
            if (tieneLetraRepetidaImparVeces(palabra)) {
                contador++;
            }
        }
        System.out.println("Hay " + contador + " palabras con una letra repetida un número impar de veces.");
    }
    
    public static boolean tieneLetraRepetidaImparVeces(String palabra) {
        for (int i = 0; i < palabra.length(); i++) {
            char letra = palabra.charAt(i);
            int contador = 0;
            for (int j = 0; j < palabra.length(); j++) {
                if (palabra.charAt(j) == letra) {
                    contador++;
                }
            }
            if (contador % 2 != 0) {
                return true;
            }
        }
        return false;
    }
}
