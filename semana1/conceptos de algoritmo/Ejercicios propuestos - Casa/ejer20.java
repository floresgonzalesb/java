public class ejer20 {
    public static void main(String[] args) {
        int contador = 0;
        for (int i = 1; i <= 999; i++) {
            int suma = 0;
            int numero = i;
            int digitos = cuentaDigitos(numero);
            while (numero > 0) {
                int digito = numero % 10;
                suma += Math.pow(digito, digitos);
                numero /= 10;
            }
            if (suma == i) {
                contador++;
            }
        }
        System.out.println("Hay " + contador + " numeros de narcisismo.");
    }
    
    public static int cuentaDigitos(int numero) {
        int digitos = 0;
        while (numero > 0) {
            digitos++;
            numero /= 10;
        }
        return digitos;
    }
}
