public class ejer7 {
    public static void main(String[] args) {
        String[] palabras = {"hello", "yellow", "success", "obvious", "access"};
        int count = 0; 
        
        for (String palabra : palabras) {
            for (int i = 0; i < palabra.length() - 2; i++) {
                char c1 = palabra.charAt(i);
                char c2 = palabra.charAt(i + 1);
                char c3 = palabra.charAt(i + 2);
                if (c1 == c2 && c2 == c3) {
                    count++;
                    break; 
                }
            }
        }
        
        System.out.println("Hay " + count + " palabras que tienen la misma letra repetida tres veces consecutivas.");
    }
}
