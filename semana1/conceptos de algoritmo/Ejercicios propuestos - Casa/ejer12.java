public class ejer12 {
    public static void main(String[] args) {
        int[] numeros = {4, 22, 27, 85, 378, 1234, 4937775};
        int contador = 0;
        for (int numero : numeros) {
            int sumaDigitos = sumaDigitos(numero);
            int sumaFactoresPrimos = sumaFactoresPrimos(numero);
            if (sumaDigitos == sumaFactoresPrimos) {
                contador++;
            }
        }
        System.out.println("Hay " + contador + " números de Smith.");
    }
    
    public static int sumaDigitos(int n) {
        int suma = 0;
        while (n != 0) {
            suma += n % 10;
            n /= 10;
        }
        return suma;
    }
    
    public static int sumaFactoresPrimos(int n) {
        int suma = 0;
        for (int i = 2; i <= n; i++) {
            while (n % i == 0) {
                suma += sumaDigitos(i);
                n /= i;
            }
        }
        return suma;
    }
}
