public class ejer13 {
    public static void main(String[] args) {
        String[] palabras = {"hola", "adios", "programacion", "computadora", "examen", "lenguaje", "repetir"};
        int contador = 0;
        for (String palabra : palabras) {
            boolean encontrada = false;
            for (int i = 0; i < palabra.length(); i++) {
                char letra = palabra.charAt(i);
                int contadorLetra = 0;
                for (int j = 0; j < palabra.length(); j++) {
                    if (palabra.charAt(j) == letra) {
                        contadorLetra++;
                    }
                }
                if (contadorLetra > 2 && contadorLetra < 5) {
                    encontrada = true;
                    break;
                }
            }
            if (encontrada) {
                contador++;
            }
        }
        System.out.println("Hay " + contador + " palabras que tienen una letra que aparece más de dos veces pero menos de cinco veces.");
    }
}
