public class ejer17 {
    public static void main(String[] args) {
        String[] palabras = {"chostito", "ropero", "tarta", "papel", "pato"};
        int contador = 0;
        for (String palabra : palabras) {
            boolean letraRepetida = false;
            for (int i = 0; i < palabra.length(); i++) {
                char letra = palabra.charAt(i);
                int contadorLetra = 0;
                for (int j = 0; j < palabra.length(); j++) {
                    if (palabra.charAt(j) == letra) {
                        contadorLetra++;
                    }
                }
                if (contadorLetra == 2) {
                    letraRepetida = true;
                    break;
                }
            }
            if (letraRepetida) {
                contador++;
            }
        }
        System.out.println("Hay " + contador + " palabras con una letra que aparece exactamente dos veces en cualquier posición.");
    }
}
