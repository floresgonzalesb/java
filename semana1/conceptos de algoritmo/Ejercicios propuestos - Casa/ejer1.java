import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class ejer1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Introduce una lista de números enteros separados por comas: ");
        int[] numeros = Arrays.stream(sc.nextLine().split(",")).mapToInt(Integer::parseInt).toArray();
        System.out.print("Introduce un número entero para buscar su suma en la lista: ");
        int objetivo = sc.nextInt();

        Set<Integer> setNumeros = new HashSet<>();
        for (int numero : numeros) {
            int complemento = objetivo - numero;
            if (setNumeros.contains(complemento)) {
                System.out.println("Hay un par de números que suman " + objetivo + ": " + numero + ", " + complemento);
                return;
            }
            setNumeros.add(numero);
        }

        System.out.println("No hay ningún par de números que sumen " + objetivo + ".");
    }
}
