import java.util.Scanner;

public class ejer3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Ingrese la cantidad de números que desea evaluar: ");
        int n = scanner.nextInt();

        int perfectos = 0;
        int fibonacci = 0;

        for (int i = 0; i < n; i++) {
            System.out.print("Ingrese un número entero: ");
            int num = scanner.nextInt();

            if (esPerfecto(num)) {
                perfectos++;
            }

            if (esFibonacci(num)) {
                fibonacci++;
            }
        }

        System.out.println("Cantidad de números perfectos: " + perfectos);
        System.out.println("Cantidad de números de Fibonacci: " + fibonacci);

        scanner.close();
    }

    public static boolean esPerfecto(int num) {
        int suma = 0;

        for (int i = 1; i < num; i++) {
            if (num % i == 0) {
                suma += i;
            }
        }

        return suma == num;
    }

    public static boolean esFibonacci(int num) {
        int a = 0;
        int b = 1;

        while (b < num) {
            int temp = a + b;
            a = b;
            b = temp;
        }

        return b == num;
    }
}
