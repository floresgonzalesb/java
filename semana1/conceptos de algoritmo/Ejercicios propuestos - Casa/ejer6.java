public class ejer6 {
    public static void main(String[] args) {
        int i = 1;
        while (true) {
            boolean divisible = true;
            for (int j = 1; j <= 20; j++) {
                if (i % j != 0) {
                    divisible = false;
                    break;
                }
            }
            if (divisible) {
                System.out.println("El número " + i + " es divisible por todos los números de 1 a 20.");
                break;
            }
            i++;
        }
    }
}
