public class ejer14 {
    public static void main(String[] args) {
        int contador = 0;
        for (int i = 1; i <= 1000; i++) {
            int cuadrado = i * i;
            String cuadradoStr = Integer.toString(cuadrado);
            for (int j = 1; j < cuadradoStr.length(); j++) {
                String parte1Str = cuadradoStr.substring(0, j);
                String parte2Str = cuadradoStr.substring(j);
                int parte1 = Integer.parseInt(parte1Str);
                int parte2 = Integer.parseInt(parte2Str);
                if (parte1 + parte2 == i) {
                    contador++;
                    break;
                }
            }
        }
        System.out.println("Hay " + contador + " números de Kaprekar.");
    }
}
