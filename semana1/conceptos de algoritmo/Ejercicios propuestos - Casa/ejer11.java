public class ejer11 {
    public static void main(String[] args) {
        String[] palabras = {"carcasa", "pinpollo", "silla", "letra"};
        int contador = 0;
        for (String palabra : palabras) {
            if (palabra.charAt(0) == palabra.charAt(3)) {
                contador++;
            }
        }
        System.out.println("Hay " + contador + " palabras que tienen la misma letra en las posiciones 1 y 4.");
    }
}
