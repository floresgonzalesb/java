public class ejer5 {
    public static void main(String[] args) {
        String[] palabras = {"hola", "murcielago", "miercoles", "aeiou"};
        int count = 0;       
        for (String palabra : palabras) {
            boolean a = false, e = false, i = false, o = false, u = false;
            for (int j = 0; j < palabra.length(); j++) {
                char c = palabra.charAt(j);
                if (c == 'a') {
                    a = true;
                } else if (c == 'e') {
                    e = true;
                } else if (c == 'i') {
                    i = true;
                } else if (c == 'o') {
                    o = true;
                } else if (c == 'u') {
                    u = true;
                }
            }
            if (a && e && i && o && u) {
                count++;
            }
        }        
        System.out.println("Hay " + count + " palabras que contienen todas las vocales.");
    }
}
