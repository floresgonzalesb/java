public class ejer16 {
    public static void main(String[] args) {
        int limiteInferior = 1;
        int limiteSuperior = 10000;
        int contador = 0;
        for (int i = limiteInferior; i <= limiteSuperior; i++) {
            int sumPotencias = 0;
            int numero = i;
            int nDigitos = Integer.toString(i).length();
            while (numero != 0) {
                int digito = numero % 10;
                sumPotencias += Math.pow(digito, nDigitos);
                numero /= 10;
            }
            if (sumPotencias == i) {
                contador++;
            }
        }
        System.out.println("Hay " + contador + " números de Armstrong entre " + limiteInferior + " y " + limiteSuperior + ".");
    }
}
