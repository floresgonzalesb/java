public class ejer10 {
    public static void main(String[] args) {
        int contador = 0;
        for (int i = 1; i <= 100000; i++) {
            if (tienePropiedad(i)) {
                contador++;
            }
        }
        System.out.println("Hay " + contador + " números enteros que tienen la propiedad de ser iguales a la suma de digitos factoriales.");
    }
    
    public static boolean tienePropiedad(int n) {
        int suma = 0;
        int digito;
        int factorial;
        int numero = n;
        while (numero != 0) {
            digito = numero % 10;
            factorial = 1;
            for (int i = 1; i <= digito; i++) {
                factorial *= i;
            }
            suma += factorial;
            numero /= 10;
        }
        return suma == n;
    }
}
