public class ejer15 {
    public static void main(String[] args) {
        String[] palabras = {"pulso", "perro", "reconocer","marciano", "poder"};
        int contador = 0;
        for (String palabra : palabras) {
            if (palabra.charAt(0) == palabra.charAt(palabra.length() - 1)) {
                contador++;
            }
        }
        System.out.println("Hay " + contador + " palabras que tienen la misma letra al principio y al final.");
    }
}
