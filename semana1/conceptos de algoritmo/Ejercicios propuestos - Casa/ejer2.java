import java.util.Scanner;

public class ejer2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Introduce una lista de palabras separadas por comas: ");
        String[] palabras = sc.nextLine().split(",");
        int contador = 0;

        for (String palabra : palabras) {
            if (palabra.matches(".*[aeiou]{3,}.*")) {
                contador++;
            }
        }

        System.out.println("Hay " + contador + " palabras con más de dos vocales consecutivas.");
        sc.close();
    }
}
