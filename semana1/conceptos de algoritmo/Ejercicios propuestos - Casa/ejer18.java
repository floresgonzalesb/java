public class ejer18 {
    public static void main(String[] args) {
        int limite = 1000;
        int contador = 0;
        for (int i = 1; i <= limite; i++) {
            int sumaDigitos = sumaDigitos(i);
            if (i % sumaDigitos == 0) {
                contador++;
            }
        }
        System.out.println("Hay " + contador + " números de Harshad entre 1 y " + limite + ".");
    }
    
    public static int sumaDigitos(int n) {
        int suma = 0;
        while (n != 0) {
            suma += n % 10;
            n /= 10;
        }
        return suma;
    }
}
