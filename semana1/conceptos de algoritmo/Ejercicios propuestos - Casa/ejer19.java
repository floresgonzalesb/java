public class ejer19 {
    public static void main(String[] args) {
        String[] palabras = {"computadora", "paralitico", "jojos", "tercio"};
        int contador = 0;
        for (String palabra : palabras) {
            if (palabra.chars().distinct().count() == palabra.length()) {
                contador++;
            }
        }
        System.out.println("Hay " + contador + " palabras con letras diferentes.");
    }
}
