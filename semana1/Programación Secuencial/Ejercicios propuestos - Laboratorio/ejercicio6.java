import java.util.Scanner;

public class ejercicio6 {
    public static void main(String[] args) {
        Scanner rc = new Scanner(System.in);

        System.out.print("Ingresa un número entero: ");
        int n = rc.nextInt();

        double raiz = Math.sqrt(n);

        System.out.println("La raíz cuadrada de " + n + " es: " + raiz);
        rc.close();
    }
}
