import java.util.Scanner;

public class ejercicio9 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Ingresa la longitud del primer lado: ");
        double lado1 = input.nextDouble();

        System.out.print("Ingresa la longitud del segundo lado: ");
        double lado2 = input.nextDouble();

        System.out.print("Ingresa la longitud del tercer lado: ");
        double lado3 = input.nextDouble();

        double hipotenusa = 0;
        double c1 = 0;
        double c2 = 0;

        if (lado1 > lado2 && lado1 > lado3) {
            hipotenusa = lado1;
            c1 = lado2;
            c2 = lado3;
        } else if (lado2 > lado1 && lado2 > lado3) {
            hipotenusa = lado2;
            c1 = lado1;
            c2 = lado3;
        } else {
            hipotenusa = lado3;
            c1 = lado1;
            c2 = lado2;
        }

        if (Math.pow(c1, 2) + Math.pow(c2, 2) == Math.pow(hipotenusa, 2)) {
            System.out.println("Los lados ingresados forman un triángulo rectángulo.");

            double seno = c1 / hipotenusa;
            double coseno = c2 / hipotenusa;
            double tangente = c1 / c2;

            System.out.println("Seno: " + seno);
            System.out.println("Coseno: " + coseno);
            System.out.println("Tangente: " + tangente);
        } else {
            System.out.println("Los lados ingresados no forman un triángulo rectángulo.");
        }
    input.close();
    }
}
