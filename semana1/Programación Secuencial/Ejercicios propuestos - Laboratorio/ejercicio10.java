import java.util.Scanner;

public class ejercicio10 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Ingrese el grado del polinomio: ");
        int grado = input.nextInt();

        double[] coeficientes = new double[grado + 1];

        for (int i = 0; i <= grado; i++) {
            System.out.print("Ingrese el coeficiente de x^" + i + ": ");
            coeficientes[i] = input.nextDouble();
        }

        String polinomio = "";

        for (int i = grado; i >= 0; i--) {
            if (coeficientes[i] != 0) {
                if (!polinomio.isEmpty()) {
                    polinomio += " + ";
                }
                if (coeficientes[i] != 1 || i == 0) {
                    polinomio += coeficientes[i];
                }
                if (i > 1) {
                    polinomio += "x^" + i;
                } else if (i == 1) {
                    polinomio += "x";
                }
            }
        }

        System.out.println("El polinomio es: " + polinomio);
    input.close();
     }
} 
 