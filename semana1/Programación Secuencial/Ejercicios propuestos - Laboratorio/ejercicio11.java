import java.util.Scanner;

public class ejercicio11 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Ingrese el nombre del vendedor: ");
        String nombre = input.nextLine();

        System.out.print("Ingrese el número de equipos exclusivos vendidos: ");
        int equipos_vendidos = input.nextInt();

        System.out.print("Ingrese el valor total facturado: ");
        double valor_facturado = input.nextDouble();

        double salario_base = 2500.0;
        double comision_por_equipo = 50.0 * equipos_vendidos;
        double comision_por_venta = 0.07 * valor_facturado;

        double salario_total = salario_base + comision_por_equipo + comision_por_venta;

        System.out.println("El salario total de " + nombre + " es: S/" + salario_total);
    input.close();
    }
}
