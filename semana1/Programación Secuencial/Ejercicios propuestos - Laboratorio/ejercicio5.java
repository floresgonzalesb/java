import java.util.Scanner;

public class ejercicio5 {
    public static void main(String[] args) {
        Scanner peri = new Scanner(System.in);

        System.out.print("Ingresa el número de lados del polígono regular: ");
        int nLados = peri.nextInt();

        System.out.print("Ingresa la longitud de cada lado del polígono regular: ");
        double longitudLado = peri.nextDouble();

        double perimetro = nLados * longitudLado;

        System.out.println("El valor del perímetro del polígono regular es: " + perimetro);

    peri.close();
    }
}
