import java.util.Scanner;

public class ejercicio3 {
    public static void main(String[] args) {
        Scanner cateto = new Scanner(System.in);

        System.out.print("Ingresa el valor del primer cateto: ");
        double c1 = cateto.nextDouble();

        System.out.print("Ingresa el valor del segundo cateto: ");
        double c2 = cateto.nextDouble();

        double hipotenusa = Math.sqrt(Math.pow(c1, 2) + Math.pow(c2, 2));

        System.out.println("El valor de la hipotenusa es: " + hipotenusa);
        cateto.close();
    }
}
