import java.util.Scanner;

public class ejercicio2 {
    public static void main(String[] args) {
        Scanner p = new Scanner(System.in);

        System.out.print("Ingresa un número positivo: ");
        int num = p.nextInt();

        boolean esPrimo = true;

        for (int i = 2; i <= Math.sqrt(num); i++) {
            if (num % i == 0) {
                esPrimo = false;
                break;
            }
        }

        if (esPrimo) {
            System.out.println(num + " es un número primo");
        } else {
            System.out.println(num + " no es un número primo");
        }
    p.close();
    }
}
