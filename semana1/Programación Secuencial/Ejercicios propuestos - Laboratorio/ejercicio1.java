import java.util.Scanner;

public class ejercicio1 {
    public static void main(String[] args) {
        Scanner elipse = new Scanner(System.in);

        System.out.print("Ingresa el valor del radio mayor (b): ");
        double b = elipse.nextDouble();

        System.out.print("Ingresa el valor del radio menor (a): ");
        double a = elipse.nextDouble();

        double area = Math.PI * a * b;
        double perimetro = Math.PI * (3 * (a + b) - Math.sqrt((3 * a + b) * (a + 3 * b)));

        System.out.println("El área de la elipse es: " + area);
        System.out.println("El perímetro de la elipse es: " + perimetro);
        elipse.close();
    }
}
