import java.util.Scanner;

public class ejercicio7 {
    public static void main(String[] args) {
        Scanner ab = new Scanner(System.in);

        System.out.print("Ingresa un número entero: ");
        int n = ab.nextInt();
        
        double valor = Math.abs(n);
        
        System.out.println("el valor absoluto de "+ n +" es:" + valor);
        ab.close();
    }
}
