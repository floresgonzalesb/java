import java.util.Scanner;

public class ejercicio13 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Ingrese una temperatura en grados Celsius: ");
        double celsius = scanner.nextDouble();

        double fahrenheit = 9/5 * celsius + 32.0;

        System.out.println(celsius + " grados Celsius equivalen a " + fahrenheit + " grados Fahrenheit.");
    scanner.close();
    }
}
