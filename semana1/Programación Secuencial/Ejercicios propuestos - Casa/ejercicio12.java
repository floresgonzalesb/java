import java.util.Scanner;

public class ejercicio12 {

    public static void main(String[] args) {
        
        Scanner input = new Scanner(System.in);
        
        System.out.print("Ingrese un número entero de 5 cifras: ");
        int numero = input.nextInt();
        
        System.out.println(numero % 10);
        System.out.println((numero / 10) % 10);
        System.out.println((numero / 100) % 10);
        System.out.println((numero / 1000) % 10);
        System.out.println(numero / 10000);

    }

}
