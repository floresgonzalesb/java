import java.util.Scanner;

public class ejercicio5 {

    public static void main(String[] args) {
        
        Scanner input = new Scanner(System.in);
        
        System.out.print("Ingrese el valor del radio de la circunferencia: ");
        double radio = input.nextDouble();

        double longitud = 2 * Math.PI * radio;
        
        double area = Math.PI * radio * radio;
    
        System.out.println("Longitud de la circunferencia: " + longitud);
        System.out.println("Area de la circunferencia: " + area);

    }

}
