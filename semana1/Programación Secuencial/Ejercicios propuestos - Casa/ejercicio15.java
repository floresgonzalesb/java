import java.util.Scanner;

public class ejercicio15 {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        System.out.print("Ingrese un número entero: ");
        int n = input.nextInt();

        System.out.print("Ingrese la cantidad de cifras a quitar: ");
        int m = input.nextInt();

        int divisor = (int) Math.pow(10, m);

        int resultado = n / divisor;

        System.out.println("El resultado después de quitar las " + m + " últimas cifras es: " + resultado);

    }
}
