import java.util.Scanner;

public class ejercicio6 {

    public static void main(String[] args) {
        
        Scanner input = new Scanner(System.in);
        
        System.out.print("Ingrese la velocidad en km/h: ");
        double velocidadKmh = input.nextDouble();
        
        double velocidadMs = velocidadKmh * 0.277778;
        
        System.out.println("La velocidad en m/s es: " + velocidadMs);

    }

}
