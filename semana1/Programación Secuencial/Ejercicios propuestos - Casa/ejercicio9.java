import java.util.Scanner;

public class ejercicio9 {

    public static void main(String[] args) {
        
        Scanner input = new Scanner(System.in);
        
        System.out.print("Ingrese la longitud del primer lado: ");
        double lado1 = input.nextDouble();
        
        System.out.print("Ingrese la longitud del segundo lado: ");
        double lado2 = input.nextDouble();
        
        System.out.print("Ingrese la longitud del tercer lado: ");
        double lado3 = input.nextDouble();
        
        double semiperimetro = (lado1 + lado2 + lado3) / 2;
        
        double area = Math.sqrt(semiperimetro * (semiperimetro - lado1) * (semiperimetro - lado2) * (semiperimetro - lado3));
        
        System.out.println("El área del triángulo es: " + area);

    }

}
