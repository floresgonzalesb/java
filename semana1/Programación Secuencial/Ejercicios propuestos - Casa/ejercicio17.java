import java.util.Scanner;

public class ejercicio17 {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        char repetir = 'S';

        while (repetir == 'S') {
            System.out.print("Ingrese la temperatura en grados Celsius: ");
            double celsius = input.nextDouble();
            double kelvin = celsius + 273.15;
            System.out.println("La temperatura en grados Kelvin es: " + kelvin);

            System.out.print("Repetir proceso? (S/N): ");
            repetir = input.next().charAt(0);
            repetir = Character.toUpperCase(repetir);
        }

        System.out.println("Programa finalizado");
    }
}
