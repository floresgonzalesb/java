import java.util.Scanner;

public class ejercicio19 {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int cantidadNumeros = 0;
        int cantidadNumerosTerminadosEnDos = 0;

        System.out.print("Ingrese un número (0 para terminar): ");
        int numero = input.nextInt();

        while (numero != 0) {
            cantidadNumeros++;
            if (numero % 10 == 2) {
                cantidadNumerosTerminadosEnDos++;
            }
            System.out.print("Ingrese un número (0 para terminar): ");
            numero = input.nextInt();
        }

        System.out.println("Cantidad de números ingresados: " + cantidadNumeros);
        System.out.println("Cantidad de números terminados en 2: " + cantidadNumerosTerminadosEnDos);
    }
}
