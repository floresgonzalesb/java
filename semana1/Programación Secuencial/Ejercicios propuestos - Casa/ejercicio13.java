import java.util.Scanner;

public class ejercicio13 {
    
    public static void main(String[] args) {
        
        Scanner input = new Scanner(System.in);
        
        System.out.print("Ingrese su fecha de nacimiento en formato dd/mm/aaaa: ");
        String fechaNacimiento = input.nextLine();
        
        int sumaCifras = 0;
        for (int i = 0; i < fechaNacimiento.length(); i++) {
            char cifra = fechaNacimiento.charAt(i);
            if (cifra >= '0' && cifra <= '9') {
                sumaCifras += cifra - '0';
            }
        }
        
        int numeroSuerte = sumaCifras;
        while (numeroSuerte > 9) {
            int sumaParcial = 0;
            while (numeroSuerte > 0) {
                sumaParcial += numeroSuerte % 10;
                numeroSuerte /= 10;
            }
            numeroSuerte = sumaParcial;
        }
        
        System.out.println("Su número de la suerte es: " + numeroSuerte);
        
    }
}
