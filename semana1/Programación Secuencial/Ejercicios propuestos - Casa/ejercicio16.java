import java.util.Scanner;

public class ejercicio16 {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        System.out.print("Ingrese la temperatura en grados Celsius: ");
        double celsius = input.nextDouble();

        double kelvin = celsius + 273.15;
        System.out.println("La temperatura en grados Kelvin es: " + kelvin);

        double reaumur = celsius * 4 / 5;
        System.out.println("La temperatura en grados Réaumur es: " + reaumur);
    }
}
