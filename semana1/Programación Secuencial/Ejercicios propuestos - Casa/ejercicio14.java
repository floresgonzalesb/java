import java.util.Scanner;

public class ejercicio14 {
    
    public static void main(String[] args) {
        
        Scanner input = new Scanner(System.in);
        
        System.out.print("Ingrese el precio de compra del producto: ");
        double precioCompra = input.nextDouble();
        
        System.out.print("Ingrese los gastos adicionales del producto: ");
        double gastosAdicionales = input.nextDouble();
        
        System.out.print("Ingrese el margen de beneficio deseado (%): ");
        double margenBeneficio = input.nextDouble() / 100.0;
        
        double precioVenta = precioCompra + gastosAdicionales + (precioCompra + gastosAdicionales) * margenBeneficio;
        
        System.out.printf("El precio final de venta del producto es: %.2f", precioVenta);
        
    }
}
