import java.util.Scanner;

public class ejercicio10 {

    public static void main(String[] args) {
        
        Scanner input = new Scanner(System.in);
        
        System.out.print("Ingrese un número de 3 cifras: ");
        int numero = input.nextInt();
        
        int primeraCifra = numero / 100;
        
        int segundaCifra = (numero / 10) % 10;
        
        int terceraCifra = numero % 10;
        
        System.out.println("La primera cifra es: " + primeraCifra);
        System.out.println("La segunda cifra es: " + segundaCifra);
        System.out.println("La tercera cifra es: " + terceraCifra);

    }

}
