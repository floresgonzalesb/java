import java.util.Scanner;

public class ejercicio20 {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Ingrese el primer número: ");
        int numero1 = input.nextInt();

        System.out.print("Ingrese el segundo número: ");
        int numero2 = input.nextInt();

        int sumaDivisoresNumero1 = 0;
        int sumaDivisoresNumero2 = 0;

        for (int i = 1; i < numero1; i++) {
            if (numero1 % i == 0) {
                sumaDivisoresNumero1 += i;
            }
        }

        for (int i = 1; i < numero2; i++) {
            if (numero2 % i == 0) {
                sumaDivisoresNumero2 += i;
            }
        }

        if (sumaDivisoresNumero1 == numero2 && sumaDivisoresNumero2 == numero1) {
            System.out.println(numero1 + " y " + numero2 + " son números amigos");
        } else {
            System.out.println(numero1 + " y " + numero2 + " no son números amigos");
        }
    }
}
