import java.util.Scanner;

public class ejercicio7 {

    public static void main(String[] args) {
        
        Scanner input = new Scanner(System.in);
        
        System.out.print("Ingrese la longitud del primer cateto: ");
        double c1 = input.nextDouble();
        
        System.out.print("Ingrese la longitud del segundo cateto: ");
        double c2 = input.nextDouble();
        
        double hipotenusa = Math.sqrt(c1 * c1 + c2 * c2);
        
        System.out.println("La longitud de la hipotenusa es: " + hipotenusa);

    }

}
