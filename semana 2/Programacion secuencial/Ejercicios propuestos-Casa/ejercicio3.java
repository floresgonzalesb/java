public class ejercicio3 {

    public static void main(String[] args) {
        
        int[][] matriz1 = crearMatriz(5, 5);
        int[][] matriz2 = crearMatriz(3, 3);

        System.out.println("Matriz 1:");
        mostrarMatriz(matriz1);
        System.out.println("Matriz 2:");
        mostrarMatriz(matriz2);

        int[][] resultado = multiplicarMatrices(matriz1, matriz2);

        System.out.println("Resultado:");
        mostrarMatriz(resultado);
    }

    public static int[][] crearMatriz(int filas, int columnas) {
        int[][] matriz = new int[filas][columnas];

        for (int i = 0; i < filas; i++) {
            for (int j = 0; j < columnas; j++) {
                matriz[i][j] = i * columnas + j + 1;
            }
        }

        return matriz;
    }

    public static void mostrarMatriz(int[][] matriz) {
        int filas = matriz.length;
        int columnas = matriz[0].length;

        for (int i = 0; i < filas; i++) {
            for (int j = 0; j < columnas; j++) {
                System.out.print(matriz[i][j] + " ");
            }
            System.out.println();
        }
    }

    public static int[][] multiplicarMatrices(int[][] matriz1, int[][] matriz2) {
        int filas1 = matriz1.length;
        int columnas1 = matriz1[0].length;
        int filas2 = matriz2.length;
        int columnas2 = matriz2[0].length;
        
        int[][] resultado = new int[filas1][columnas2];

        for (int i = 0; i < filas1; i++) {
            for (int j = 0; j < columnas2; j++) {
                int sum = 0;
                for (int k = 0; k < columnas1; k++) {
                    sum += matriz1[i][k] * matriz2[k][j];
                }
                resultado[i][j] = sum;
            }
        }

        return resultado;
    }
}
