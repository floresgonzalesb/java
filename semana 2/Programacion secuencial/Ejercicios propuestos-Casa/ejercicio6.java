import java.util.Arrays;
import java.util.Collections;

public class ejercicio6 {

    public static void main(String[] args) {
        String[] arreglo = {"perro", "gato", "elefante", "jirafa", "cebra"};
        Arrays.sort(arreglo);
        Arrays.sort(arreglo, Collections.reverseOrder());
        System.out.println(Arrays.toString(arreglo));
    }
}
