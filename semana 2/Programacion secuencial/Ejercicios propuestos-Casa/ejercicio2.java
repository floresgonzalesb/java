import java.util.Random;

public class ejercicio2 {
    public static void main(String[] args) {

        int[][] matriz = crearMatriz(7, 7);

        System.out.println("Matriz creada:");
        mostrarMatriz(matriz);
    }

    public static int[][] crearMatriz(int filas, int columnas) {
        int[][] matriz = new int[filas][columnas];

        Random rand = new Random();
        for (int i = 0; i < filas; i++) {
            for (int j = 0; j < columnas; j++) {
                matriz[i][j] = rand.nextInt(100) + 1;
            }
        }

        return matriz;
    }

    public static void mostrarMatriz(int[][] matriz) {
        int filas = matriz.length;
        int columnas = matriz[0].length;

        for (int i = 0; i < filas; i++) {
            for (int j = 0; j < columnas; j++) {
                System.out.print(matriz[i][j] + " ");
            }
            System.out.println();
        }
    }
}
