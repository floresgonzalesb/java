import java.util.Arrays;

public class ejercicio9 {

    public static void main(String[] args) {
        int[] arreglo = {5, 3, 9, 1, 7};
        Arrays.sort(arreglo);
        double mediana;
        if (arreglo.length % 2 == 0) {
            int indice1 = arreglo.length/2 - 1;
            int indice2 = arreglo.length/2;
            mediana = (arreglo[indice1] + arreglo[indice2]) / 2.0;
        } else {
            int indice = arreglo.length/2;
            mediana = arreglo[indice];
        }
        System.out.println("La mediana del arreglo es: " + mediana);
    }
}
