import java.util.Arrays;

public class ejercicio17 {

    public static void main(String[] args) {
        int[] arreglo = {1, 2, 3, 4, 5, 6, 7, 8};
        int mitad = arreglo.length / 2;
        int[] primerMitad = new int[mitad];
        int[] segundaMitad = new int[mitad];

        for (int i = 0; i < mitad; i++) {
            primerMitad[i] = arreglo[i];
        }

        for (int i = mitad; i < arreglo.length; i++) {
            segundaMitad[i - mitad] = arreglo[i];
        }

        System.out.println("Primer mitad: " + Arrays.toString(primerMitad));
        System.out.println("Segunda mitad: " + Arrays.toString(segundaMitad));
    }
}
