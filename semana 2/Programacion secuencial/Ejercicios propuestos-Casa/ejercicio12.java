public class ejercicio12 {

    public static void main(String[] args) {
        int[] arreglo = {5, 3, 9, 1, 7};
        int maximo = arreglo[0];
        for (int i = 1; i < arreglo.length; i++) {
            if (arreglo[i] > maximo) {
                maximo = arreglo[i];
            }
        }
        System.out.println("El número más grande del arreglo es: " + maximo);
    }
}
