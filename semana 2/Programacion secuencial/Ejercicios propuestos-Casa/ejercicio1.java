public class ejercicio1 {
    public static void main(String[] args) {

        int[][] matriz = crearMatriz(7, 7);

        System.out.println("Matriz original:");
        mostrarMatriz(matriz);

        int[][] matrizRotada = rotarMatriz(matriz);

        System.out.println("Matriz rotada:");
        mostrarMatriz(matrizRotada);
    }

    public static int[][] crearMatriz(int filas, int columnas) {
        int[][] matriz = new int[filas][columnas];

        for (int i = 0; i < filas; i++) {
            for (int j = 0; j < columnas; j++) {
                matriz[i][j] = i*columnas + j + 1;
            }
        }

        return matriz;
    }

    public static int[][] rotarMatriz(int[][] matriz) {
        int filas = matriz.length;
        int columnas = matriz[0].length;

        int[][] matrizRotada = new int[columnas][filas];

        for (int i = 0; i < filas; i++) {
            for (int j = 0; j < columnas; j++) {
                matrizRotada[j][filas-1-i] = matriz[i][j];
            }
        }

        return matrizRotada;
    }

    public static void mostrarMatriz(int[][] matriz) {
        int filas = matriz.length;
        int columnas = matriz[0].length;

        for (int i = 0; i < filas; i++) {
            for (int j = 0; j < columnas; j++) {
                System.out.print(matriz[i][j] + " ");
            }
            System.out.println();
        }
    }
}
