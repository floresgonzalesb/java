public class ejercicio11 {

    public static void main(String[] args) {
        int[] arreglo = {5, 3, 9, 1, 7};
        int suma = 0;
        for (int i = 0; i < arreglo.length; i++) {
            suma += arreglo[i];
        }
        double media = (double) suma / arreglo.length;
        System.out.println("La media del arreglo es: " + media);
    }
}