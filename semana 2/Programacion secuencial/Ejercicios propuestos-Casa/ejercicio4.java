import java.util.Random;

public class ejercicio4 {

  public static void main(String[] args) {

    int[][] matriz = new int[7][7];
    Random rnd = new Random();

    for (int i = 0; i < 7; i++) {
      for (int j = 0; j < 7; j++) {
        matriz[i][j] = rnd.nextInt(10);
      }
    }

    for (int i = 0; i < 7; i++) {
      for (int j = 0; j < 7; j++) {
        System.out.print(matriz[i][j] + " ");
      }
      System.out.println();
    }
  }
}
