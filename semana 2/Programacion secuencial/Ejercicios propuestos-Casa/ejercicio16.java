import java.util.*;

public class ejercicio16 {

    public static void main(String[] args) {
        int[] arreglo = {3, 5, 2, 5, 6, 3, 2, 5, 5};
        HashMap<Integer, Integer> ocurrencias = new HashMap<Integer, Integer>();

        for (int i = 0; i < arreglo.length; i++) {
            if (ocurrencias.containsKey(arreglo[i])) {
                ocurrencias.put(arreglo[i], ocurrencias.get(arreglo[i]) + 1);
            } else {
                ocurrencias.put(arreglo[i], 1);
            }
        }

        int maxValor = 0;
        int maxElemento = 0;
        for (Map.Entry<Integer, Integer> entry : ocurrencias.entrySet()) {
            if (entry.getValue() > maxValor) {
                maxValor = entry.getValue();
                maxElemento = entry.getKey();
            }
        }

        System.out.println("El número que se repite más veces es " + maxElemento + ", con una frecuencia de " + maxValor);
    }
}
