public class ejercicio20{

    public static void main(String[] args) {
        char[] asciiArray = createAsciiArray();
        printAsciiArray(asciiArray);
    }

    public static char[] createAsciiArray() {
        char[] asciiArray = new char[256];
        for (int i = 0; i < 256; i++) {
            asciiArray[i] = (char) i;
        }
        return asciiArray;
    }

    public static void printAsciiArray(char[] asciiArray) {
        for (char c : asciiArray) {
            System.out.print(c + " ");
        }
    }
}
