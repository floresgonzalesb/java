import java.util.Arrays;

public class ejercicio5 {

    public static int[] subarregloCreciente(int[] arreglo) {
        int n = arreglo.length;
        int inicio = 0;
        int fin = 0;
        int inicioActual = 0;
        int finActual = 0;

        for (int i = 1; i < n; i++) {
            if (arreglo[i] > arreglo[i-1]) {
                finActual = i;
            } else {
                if (finActual - inicioActual > fin - inicio) {
                    inicio = inicioActual;
                    fin = finActual;
                }
                inicioActual = i;
                finActual = i;
            }
        }

        if (finActual - inicioActual > fin - inicio) {
            inicio = inicioActual;
            fin = finActual;
        }

        return Arrays.copyOfRange(arreglo, inicio, fin+1);
    }

    public static void main(String[] args) {
        int[] arreglo = {1, 2, 3, 1, 2, 3, 4, 5, 6, 2, 3, 4, 1, 2, 3, 4, 5};
        int[] subarreglo = subarregloCreciente(arreglo);
        System.out.println(Arrays.toString(subarreglo));
    }
}
