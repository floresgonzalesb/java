public class ejercicio18 {
    public static void main(String[] args) {
        int[] arreglo1 = {1, 2, 3};
        int[] arreglo2 = {4, 5, 6};

        int resultado = 0;
        for (int i = 0; i < arreglo1.length; i++) {
            resultado += arreglo1[i] * arreglo2[i];
        }

        System.out.println("El producto punto es: " + resultado);
    }
}

