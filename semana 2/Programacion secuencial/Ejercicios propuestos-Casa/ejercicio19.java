import java.util.Scanner;

public class ejercicio19 {

    public static void main(String[] args) {
        Scanner sc= new Scanner(System.in);

        System.out.println("ingrese un caracter: ");
        char letra = sc.next().charAt(0);
        boolean esVocal = false;
        
        switch (letra) {
            case 'a':
            case 'e':
            case 'i':
            case 'o':
            case 'u':
                esVocal = true;
                break;
            default:
                esVocal = false;
                break;
        }

        if (esVocal) {
            System.out.println(letra + " es una vocal");
        } else {
            System.out.println(letra + " es una consonante");
        }
    }
}
