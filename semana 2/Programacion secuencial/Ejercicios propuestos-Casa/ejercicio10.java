import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class ejercicio10 {

    public static void main(String[] args) {
        int[] arreglo = {5, 3, 9, 1, 7, 5, 3, 5, 3};
        Map<Integer, Integer> mapa = new HashMap<Integer, Integer>();
        for (int i = 0; i < arreglo.length; i++) {
            int elemento = arreglo[i];
            if (mapa.containsKey(elemento)) {
                int frecuencia = mapa.get(elemento);
                mapa.put(elemento, frecuencia + 1);
            } else {
                mapa.put(elemento, 1);
            }
        }
        int moda = 0;
        int maxFrecuencia = 0;
        for (Map.Entry<Integer, Integer> entry : mapa.entrySet()) {
            int elemento = entry.getKey();
            int frecuencia = entry.getValue();
            if (frecuencia > maxFrecuencia) {
                moda = elemento;
                maxFrecuencia = frecuencia;
            }
        }
        System.out.println("La moda del arreglo es: " + moda);
    }
}
