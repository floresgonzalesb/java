import java.util.Arrays;

public class ejercicio7 {

    public static void main(String[] args) {
        int[] arreglo = {10, 5, 8, 3, 2, 4};
        int k = 3;
        Arrays.sort(arreglo);
        int kEsimoElemento = arreglo[arreglo.length - k];
        System.out.println("El k-ésimo elemento más grande es: " + kEsimoElemento);
    }
}
