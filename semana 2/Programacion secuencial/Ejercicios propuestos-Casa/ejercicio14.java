import java.util.Arrays;

public class ejercicio14 {

    public static void main(String[] args) {
        int[] arreglo = {5, 3, 9, 1, 7, 3, 9};
        Arrays.sort(arreglo);
        int numeroBuscado = 9;
        int contador = 0;
        int indice = 0;
        while (indice >= 0) {
            indice = busquedaBinaria(arreglo, numeroBuscado, indice);
            if (indice >= 0) {
                contador++;
                indice++;
            }
        }
        System.out.println("El número de ocurrencias de " + numeroBuscado + " en el arreglo es: " + contador);
    }

    public static int busquedaBinaria(int[] arreglo, int valorBuscado, int inicio) {
        int fin = arreglo.length - 1;
        while (inicio <= fin) {
            int medio = (inicio + fin) / 2;
            if (arreglo[medio] < valorBuscado) {
                inicio = medio + 1;
            } else if (arreglo[medio] > valorBuscado) {
                fin = medio - 1;
            } else {
                return medio;
            }
        }
        return -1;
    }
}
