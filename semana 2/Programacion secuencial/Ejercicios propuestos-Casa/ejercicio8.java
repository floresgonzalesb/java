public class ejercicio8 {
    public static void main(String[] args) {
        int[] arreglo = {3, 7, 2, 8, 5, 1};
        int contadorParejas = 0;
        for (int i = 0; i < arreglo.length; i++) {
            for (int j = i + 1; j < arreglo.length; j++) {
                if (esPrimo(arreglo[i] + arreglo[j])) {
                    contadorParejas++;
                }
            }
        }
        System.out.println("El número de parejas de elementos cuya suma es un número primo es: " + contadorParejas);
    }
    
    public static boolean esPrimo(int numero) {
        if (numero <= 1) {
            return false;
        }
        for (int i = 2; i <= numero / 2; i++) {
            if (numero % i == 0) {
                return false;
            }
        }
        return true;
    }
}
