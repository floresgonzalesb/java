public class ejercicio13 {

    public static void main(String[] args) {
        int[] arreglo = {5, 3, 9, 1, 7};
        int contador = 0;
        for (int i = 0; i < arreglo.length; i++) {
            if (arreglo[i] % 2 != 0) {
                contador++;
            }
        }
        System.out.println("El número de elementos impares en el arreglo es: " + contador);
    }
}
