public class ejercicio10 {

    public static void main(String[] args) {
        int[] numeros = {10, 5, 7, 3, 8};
        imprimirArreglo(numeros);
        ondenDecendente(numeros);
        imprimirArreglo(numeros);
    }

    public static void ondenDecendente(int[] arreglo) {
        int n = arreglo.length;
        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < n - i - 1; j++) {
                if (arreglo[j] < arreglo[j + 1]) {
                    int temp = arreglo[j];
                    arreglo[j] = arreglo[j + 1];
                    arreglo[j + 1] = temp;
                }
            }
        }
    }

    public static void imprimirArreglo(int[] arreglo) {
        System.out.print("[");
        for (int i = 0; i < arreglo.length; i++) {
            System.out.print(arreglo[i]);
            if (i != arreglo.length - 1) {
                System.out.print(", ");
            }
        }
        System.out.println("]");
    }

}
