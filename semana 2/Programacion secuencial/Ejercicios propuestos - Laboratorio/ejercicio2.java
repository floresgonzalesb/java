public class ejercicio2 {

    public static int mA(int[] arreglo) {
        int producto = 1;
        for (int i = 0; i < arreglo.length; i++) {
            producto *= arreglo[i];
        }
        return producto;
    }

    public static void main(String[] args) {
        int[] miArreglo = {1, 2, 3, 4, 5};
        int producto = mA(miArreglo);
        System.out.println("El producto es: " + producto);
    }
}
