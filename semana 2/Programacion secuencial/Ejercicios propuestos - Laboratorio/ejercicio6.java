import java.math.BigInteger;
import java.util.Scanner;

public class ejercicio6 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Cuantos numeros ingresara?: ");
        int numeros = sc.nextInt();
        BigInteger[] resultados = new BigInteger[numeros];

        for (int i = 0; i < numeros; i++) {
            System.out.print("Ingresa un numero menor o igual a 100 para calcular su factorial: ");
            int n = sc.nextInt();
            BigInteger factorial = BigInteger.ONE;
            for (int j = 1; j <= n; j++) {
                factorial = factorial.multiply(BigInteger.valueOf(j));
            }
            resultados[i] = factorial;
        }

        System.out.println("Salida:");
        for (int i = 0; i < numeros; i++) {
            System.out.println("El factorial de " + (i+1) + " es: " + resultados[i]);
        }
    }
}
    