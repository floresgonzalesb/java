import java.util.Scanner;

public class ejercicio1 {

    public static int cSuma(int[] arreglo) {
        int suma = 0;
        for (int i = 0; i < arreglo.length; i++) {
            suma += arreglo[i];
        }
        return suma;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Ingrese la cantidad del arreglo: ");
        int cantidad = sc.nextInt();

        int[] miArreglo = new int[cantidad];

        for (int i = 0; i < miArreglo.length; i++) {
            System.out.print("Ingrese el elemento " + (i + 1) + ": ");
            miArreglo[i] = sc.nextInt();
        }

        int suma = cSuma(miArreglo);

        System.out.println("La suma es: " + suma);

    }
}
