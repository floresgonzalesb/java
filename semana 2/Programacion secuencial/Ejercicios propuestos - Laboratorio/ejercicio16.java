import java.util.ArrayList;
import java.util.Scanner;

public class ejercicio16 {

    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        ArrayList<String> nombres = new ArrayList<String>();
        ArrayList<Integer> edades = new ArrayList<Integer>();

        System.out.println("Introduce los nombres y edades de los alumnos:");
        String nombre;
        int edad;
        while (true) {
            System.out.print("Nombre: ");
            nombre = entrada.nextLine();
            if (nombre.equals("*")) {
                break;
            }
            System.out.print("Edad: ");
            edad = entrada.nextInt();
            entrada.nextLine(); 
            nombres.add(nombre);
            edades.add(edad);
        }

        System.out.println("Alumnos mayores de edad:");
        for (int i = 0; i < nombres.size(); i++) {
            if (edades.get(i) >= 18) {
                System.out.println(nombres.get(i));
            }
        }

        int maxEdad = -1;
        ArrayList<String> mayores = new ArrayList<String>();
        for (int i = 0; i < nombres.size(); i++) {
            if (edades.get(i) > maxEdad) {
                maxEdad = edades.get(i);
                mayores.clear();
                mayores.add(nombres.get(i));
            } else if (edades.get(i) == maxEdad) {
                mayores.add(nombres.get(i));
            }
        }
        System.out.println("Alumnos mayores:");
        for (String alumno : mayores) {
            System.out.println(alumno);
        }
    }

}
