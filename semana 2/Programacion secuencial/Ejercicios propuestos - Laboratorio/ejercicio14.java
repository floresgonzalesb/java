import java.util.Scanner;

public class ejercicio14 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ingresa un numero entre 1000 y 7000: ");
        int num = scanner.nextInt();
scanner.close();
        if (num < 1000 || num > 7000) {
            System.out.println("El numero ingresado no esta en el rango adecuado.");
            return;
        }

        int[] digitos = new int[4];
        for (int i = 0; i < 4; i++) {
            digitos[i] = num % 10;
            num /= 10;
        }

        for (int i = 3; i >= 0; i--) {
            System.out.println("[" + i + "] = " + digitos[i]);
        }
    }
}
