import java.util.Scanner;

class ejemplo_01{

    void lectura_teclado(){
        
    }

    int suma_numeros(int n){
        int[] numero = new int[n];
        int suma = 0;
        Scanner leer_numero = new Scanner(System.in);

        for(int i=0 ; i < n; i++ ){
            numero[i] = leer_numero.nextInt();
            suma = suma + numero[i];
        }
        return suma;
    }

    public static void main(String[] args){
        Scanner leer_numero = new Scanner(System.in);

        int n;

        System.out.println("¿Cuántos números ingresara?: ");
        n = leer_numero.nextInt();

        ejemplo_01 suma_numeros = new ejemplo_01();

        System.out.println("La suma de números es: " + suma_numeros.suma_numeros(n));
    }

}
