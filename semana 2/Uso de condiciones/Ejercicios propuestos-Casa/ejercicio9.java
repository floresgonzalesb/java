import java.util.Scanner;

public class ejercicio9 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese el valor de n: ");
        int n = sc.nextInt();
        System.out.print("Ingrese el valor del primer término: ");
        double a1 = sc.nextDouble();
        System.out.print("Ingrese la razón: ");
        double r = sc.nextDouble();
        double suma = a1 * (1 - Math.pow(r, n)) / (1 - r);
        System.out.println("La suma de los primeros " + n + " términos de la serie geométrica es: " + suma);
    }
}
