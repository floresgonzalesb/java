public class ejercicio15 {
    public static void main(String[] args) {
        double prestamo = 5000.0;
        double tasaInteresMensual = 0.016;
        double pagoMensual = 0.0;
        
        for (int mes = 1; mes <= 18; mes++) {
            pagoMensual = prestamo * tasaInteresMensual / (1 - Math.pow(1 + tasaInteresMensual, -18));
            prestamo = prestamo * (1 + tasaInteresMensual) - pagoMensual;
            System.out.printf("Mes %d: saldo restante = %.2f soles\n", mes, prestamo);
        }
    }
}
