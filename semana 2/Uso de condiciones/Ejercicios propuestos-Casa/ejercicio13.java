import java.util.Scanner;

public class ejercicio13 {
    public static void main(String[] args) throws InterruptedException {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese la cantidad de segundos: ");
        int segundos = sc.nextInt();
        
        for (int i = segundos; i >= 0; i--) {
            System.out.print("\r" + i);
            Thread.sleep(1000);
        }
        
        System.out.println("\n¡Tiempo finalizado!");
    }
}