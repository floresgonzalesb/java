public class ejercicio4 {
    public static void main(String[] args) {
        for (int n = 1; n <= 10; n++) {
            int factorial = 1;
            for (int i = 2; i <= n; i++) {
                factorial *= i;
            }
            System.out.println("El factorial de " + n + " es " + factorial);
        }
    }
}
