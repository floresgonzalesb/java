public class ejercicio3 {
    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("Debe introducir un único argumento numérico");
            return;
        }
        int n = Integer.parseInt(args[0]);
        if (n < 0) {
            System.out.println("El argumento debe ser un número positivo");
            return;
        }
        int factorial = 1;
        for (int i = 2; i <= n; i++) {
            factorial *= i;
        }
        System.out.println("El factorial de " + n + " es " + factorial);
    }
}
