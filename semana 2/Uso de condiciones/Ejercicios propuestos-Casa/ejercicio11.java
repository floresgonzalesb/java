public class ejercicio11 {
    public static void main(String[] args) {
        double pagoMensual = 0;
        double totalPagado = 0;
        double montoActual = 10;
        
        for (int i = 1; i <= 20; i++) {
            pagoMensual += montoActual;
            totalPagado += montoActual;
            montoActual *= 2;
        }
        
        pagoMensual /= 20;
        
        System.out.println("El pago mensual es: S/ " + pagoMensual);
        System.out.println("El total pagado después de 20 meses es: S/ " + totalPagado);
    }
}
