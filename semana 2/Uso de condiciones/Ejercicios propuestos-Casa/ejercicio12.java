import java.util.Scanner;

public class ejercicio12 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese la cantidad de números primos a mostrar: ");
        int n = sc.nextInt();
        
        int count = 0;
        int i = 2;
        while(count < n) {
            if(esPrimo(i)) {
                System.out.print(i + " ");
                count++;
            }
            i++;
        }
    }
    
    public static boolean esPrimo(int num) {
        if(num <= 1) {
            return false;
        }
        for(int i = 2; i <= Math.sqrt(num); i++) {
            if(num % i == 0) {
                return false;
            }
        }
        return true;
    }
}
