import java.util.Scanner;

public class ejercicio7 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese el número de términos que desea imprimir: ");
        int n = sc.nextInt();
        System.out.println("Sucesión de Ulam para los primeros " + n + " términos:");
        int[] sequence = new int[n];
        sequence[0] = 1;
        sequence[1] = 2;
        for (int i = 2; i < n; i++) {
            int count = 0;
            for (int j = 0; j < i; j++) {
                int diff = sequence[i] - sequence[j];
                for (int k = j + 1; k < i; k++) {
                    if (sequence[k] == diff) {
                        count++;
                        break;
                    }
                }
            }
            if (count == 1) {
                sequence[i] = sequence[i - 1] + 1;
            } else {
                int j = i - 1;
                while (j >= 0 && count > 1) {
                    int diff = sequence[i] - sequence[j];
                    int k = j - 1;
                    while (k >= 0) {
                        if (sequence[k] == diff) {
                            count--;
                            break;
                        }
                        k--;
                    }
                    j--;
                }
                sequence[i] = sequence[i - 1] + 1;
                while (j >= 0) {
                    int diff = sequence[i] - sequence[j];
                    boolean found = false;
                    for (int k = j - 1; k >= 0; k--) {
                        if (sequence[k] == diff) {
                            found = true;
                            break;
                        }
                    }
                    if (!found) {
                        sequence[i] = sequence[j] + diff;
                        break;
                    }
                    j--;
                }
            }
        }
        for (int i = 0; i < n; i++) {
            System.out.print(sequence[i] + " ");
        }
    }
}
