import java.util.Scanner;

public class ejercicio_3 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese el número en base decimal que desea convertir: ");
        int decimal = sc.nextInt();
        System.out.print("Ingrese el numeo que desea convertir (entre 2 y 9): ");
        int base = sc.nextInt();
        String numeroConvertido = convertirABase(decimal, base);
        System.out.println(decimal + " en base " + base + " es igual a " + numeroConvertido);
        sc.close();
    }

    public static String convertirABase(int decimal, int base) {
        String resultado = "";
        while (decimal > 0) {
            int residuo = decimal % base;
            resultado = residuo + resultado;
            decimal /= base;
        }
        return resultado; 
    }
}
