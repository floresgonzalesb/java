import java.util.Scanner;

public class ejercicio_9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ingrese un número entero positivo: ");
        int numero = scanner.nextInt();

        int suma = 0;
        int divisor = 1;

        do {
            if (numero % divisor == 0) {
                suma += divisor;
            }
            divisor++;
        } while (divisor <= numero/2);

        if (suma == numero) {
            System.out.println(numero + " es un número perfecto.");
        } else {
            System.out.println(numero + " no es un número perfecto.");
        }
        scanner.close();
    }
}