import java.util.Scanner;

public class ejercicio_1 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese el número límite de la secuencia de Fibonacci: ");
        int limite = sc.nextInt();
        int n1 = 0, n2 = 1;
        System.out.print(n1 + " " + n2);
        int suma = n1 + n2;
        while(suma <= limite) {
            System.out.print(" " + suma);
            n1 = n2;
            n2 = suma;
            suma = n1 + n2;
        sc.close();
        }
    }
}