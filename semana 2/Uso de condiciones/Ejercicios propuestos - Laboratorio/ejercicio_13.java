import java.util.Scanner;

public class ejercicio_13 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Introduce la cantidad de productos: ");
        int n = sc.nextInt();
        
        String[] productos = new String[n];
        for (int i = 0; i < n; i++) {
            System.out.print("Introduce el producto " + (i+1) + ": ");
            productos[i] = sc.next();
        }
        
        System.out.println("\nLos productos introducidos son: ");
        for (int i = 0; i < n; i++) {
            System.out.println((i+1) + ". " + productos[i]);
        }
        sc.close();
    }
}

