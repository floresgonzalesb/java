import java.util.Scanner;

public class ejercicio_15 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Introduce el número límite de la secuencia Lucas: ");
        int limite = sc.nextInt();
        
        int a = 2, b = 1;
        System.out.print(a + " " + b + " ");
        int c = a + b;
        while (c <= limite) {
            System.out.print(c + " ");
            a = b;
            b = c;
            c = a + b;
        }
        sc.close();
    }
}
