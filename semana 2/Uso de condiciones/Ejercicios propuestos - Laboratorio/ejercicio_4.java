import java.util.Scanner;

public class ejercicio_4 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese el número de números perfectos que desea sumar: ");
        int n = sc.nextInt();
        int suma = 0;
        int contador = 1;
        int numPerfectos = 0;
        while (numPerfectos < n) {
            if (esPerfecto(contador)) {
                suma += contador;
                numPerfectos++;
            }
            contador++;
        }
        System.out.println("La suma de los primeros " + n + " números perfectos es: " + suma);
    sc.close();
    }

    public static boolean esPerfecto(int num) {
        int sumaDivisores = 0;
        for (int i = 1; i < num; i++) {
            if (num % i == 0) {
                sumaDivisores += i;
            }
        }
        return sumaDivisores == num;
    }
}
