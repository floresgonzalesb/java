import java.util.Scanner;

public class ejercicio_7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ingrese la cantidad de números pares a sumar: ");
        
        int n = scanner.nextInt();
        int sum = 0;
        int count = 0;
        int i = 2;

        do {
            sum += i;
            i += 2;
            count++;
        } while (count < n);

        System.out.println("La suma de los " + n + " primeros números pares es: " + sum);
        scanner.close();
    }
}
