import java.util.Scanner;

public class ejercicio_8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ingrese la cantidad de números: ");
        int n = scanner.nextInt();
        int[] numeros = new int[n];

        for (int i = 0; i < n; i++) {
            System.out.print("Ingrese el número #" + (i+1) + ": ");
            numeros[i] = scanner.nextInt();
        }

        int mcd = numeros[0];
        int i = 1;

        while (i < n && mcd != 1) {
            int numeroActual = numeros[i];
            while (numeroActual != 0) {
                int temp = mcd % numeroActual;
                mcd = numeroActual;
                numeroActual = temp;
            }
            i++;
        }

        System.out.println("El Máximo Común Divisor es: " + mcd);
        scanner.close();
    }
}
