import java.util.Scanner;

public class ejercicio_17 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Introduce el tamaño del lado del cuadrado: ");
        int lado = sc.nextInt();
        
        for (int i = 1; i <= lado; i++) {
            for (int j = 1; j <= lado; j++) {
                if (i == 1 || i == lado || j == 1 || j == lado) {
                    System.out.print("* ");
                } else {
                    System.out.print("* ");
                }
            }
            System.out.println();
        }
    sc.close();
    }
}

