import java.util.Scanner;

public class ejercicio_12 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Introduce la cantidad de números: ");
        int n = sc.nextInt();
        
        int[] numeros = new int[n];
        for (int i = 0; i < n; i++) {
            System.out.print("Introduce el número " + (i+1) + ": ");
            numeros[i] = sc.nextInt();
        }
        
        int mcm = calcularMCM(numeros);
        System.out.println("El Mínimo Común Múltiplo de los " + n + " números es: " + mcm);
        sc.close();
    }
    
    public static int calcularMCM(int[] numeros) {
        int maximo = numeros[0];
        for (int i = 1; i < numeros.length; i++) {
            if (numeros[i] > maximo) {
                maximo = numeros[i];
            }
        }
        
        int mcm = maximo;
        boolean encontrado = false;
        while (!encontrado) {
            int i;
            for (i = 0; i < numeros.length; i++) {
                if (mcm % numeros[i] != 0) {
                    break;
                }
            }
            if (i == numeros.length) {
                encontrado = true;
            } else {
                mcm += maximo;
            }
        }
        
        return mcm;
    }
}
