import java.util.Scanner;

public class ejercicio_2 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese el número de impares que desea sumar: ");
        int n = sc.nextInt();
        int suma = 0;
        int contador = 1;
        int numImpares = 0;
        while (numImpares < n) {
            if (contador % 2 != 0) {
                suma += contador;
                numImpares++;
            }
            contador++;
        }
        System.out.println("La suma de los " + n + " primeros números impares es: " + suma);
        sc.close();
    }
}
