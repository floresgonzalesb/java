import java.util.Scanner;

public class ejercicio_6 {
    public static void main(String[] args) {
        Scanner  tri = new Scanner(System.in);
        System.out.println("Ingrese la cantidad del triangulo: ");
        int n= tri.nextInt();
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= i; j++) {
                System.out.print("*");
            }
            System.out.println();
        }
        tri.close();
    }
}
